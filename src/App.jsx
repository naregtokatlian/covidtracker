import React, { Component } from "react";
import { CountryPicker, Chart, Cards } from "./components";
import { fetchData } from "./api";
import styles from "./App.module.css";

class App extends Component {
  state = {
    data: {},
    country: "",
  };

  handleCountryChange = async (country) => {
    const fetchedData = await fetchData(country);
    console.log(fetchedData);
    this.setState({
      data: fetchedData,
      country: country,
    });
  };
  async componentDidMount() {
    const fetchedData = await fetchData();
    this.setState({
      data: fetchedData,
    });
  }
  render() {
    const { data, country } = this.state;
    return (
      
        <div className={styles.container}>
        <h1>Corona Go--- Go Corona</h1>
          <Cards data={data} />
          <CountryPicker
            handleCountryChange={this.handleCountryChange}
          ></CountryPicker>
          <Chart data={data} country={country}>
            
          </Chart>
        </div>
      
    );
  }
}
export default App;
