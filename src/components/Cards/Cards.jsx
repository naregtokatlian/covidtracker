import React from 'react'
import { Card, Grid, Typography, CardContent } from '@material-ui/core'
import CountUp from 'react-countup';
import styles from './Cards.module.css';
import cx from 'classnames';


export default function Cards({ data: { confirmed, deaths, recovered, lastUpdate } }) {
    if (!confirmed) {
        return "Loading...."
    }
    return (
        <div>
            <Grid container spacing={3} justify='center'>
                <Grid item component={Card} xs={12} md={3} className={cx(styles.card, styles.infected)}>
                    <CardContent>
                        <Typography color='textSecondary' gutterBottom>Infected</Typography>
                        <Typography varient="h5">
                            <CountUp start={0} end={confirmed.value} separator=' ,' duration={2.5} />
                        </Typography>
                        <Typography color='textSecondary'>{new Date(lastUpdate).toDateString()}</Typography>
                        <Typography varient="h5">Number of infected People from Covid-19</Typography>
                    </CardContent>
                </Grid>
                <Grid item component={Card} xs={12} md={3} className={cx(styles.card, styles.recovered)}>
                    <CardContent>
                        <Typography color='textSecondary' gutterBottom>Recoved</Typography>
                        <Typography varient="h5">
                            <CountUp start={0} end={recovered.value} separator=' ,' duration={2.5} />
                        </Typography>
                        <Typography color='textSecondary'>{new Date(lastUpdate).toDateString()}</Typography>
                        <Typography varient="h5">Number of Recoved People from Covid-19</Typography>
                    </CardContent>
                </Grid>
                <Grid item component={Card} xs={12} md={3} className={cx(styles.card, styles.deaths)}>
                    <CardContent>
                        <Typography color='textSecondary' gutterBottom>Deaths</Typography>
                        <Typography varient="h5">
                            <CountUp start={0} end={deaths.value} duration={2.5} separator=' ,' />
                        </Typography>
                        <Typography color='textSecondary'>{new Date(lastUpdate).toDateString()}</Typography>
                        <Typography varient="h5">Number of People who Died from Covid-19</Typography>
                    </CardContent>
                </Grid>
            </Grid>
        </div >
    )
}


