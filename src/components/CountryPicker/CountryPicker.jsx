import React, { useState, useEffect } from "react";
import { countriesAPI } from "../../api";
import styles from "./CountryPicker.module.css";
import { FormControl, NativeSelect } from "@material-ui/core";

export default function CountryPicker({ handleCountryChange }) {
  const [fetchedCountries, setfetchedCountries] = useState([]);
  useEffect(() => {
    const fetchCountries = async () => {
      setfetchedCountries(await countriesAPI());
    };
    fetchCountries();
  }, []);

  return (
    <div className={styles.formControl}>
      <FormControl className={styles.formControl}>
        <NativeSelect className={styles.selectCountries}
          defaultValue=""
          onChange={(e) => {
            handleCountryChange(e.target.value);
          }}
        >
            <option value="">Global</option>
          {fetchedCountries.map((country, index) => (
            <option key={index} value={country}>
              {country}
            </option>
          ))}
        </NativeSelect>
      </FormControl>
    </div>
  );
}
